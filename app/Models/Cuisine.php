<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{
    protected $table    =   'cuisines';

    protected $fillable = [
        'name',  'is_active', 'deleted_at', 'created_at', 'updated_at'
    ];

    public function scopeSearch($query, $term)
    {
        return $query->where(function ($query) use ($term) {
            $query->where('name', 'like', '%'.$term.'%');
        });
    }

}

