<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;

use App\Domains\Auth\Models\User; 
use App\Models\Product;
use App\Models\Cms\PickList;
use App\Models\Cms\Lists;
use App\Models\Cms\ListLink;



use Illuminate\Support\Str; 

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Mail;
use URL;
use DB; 
use Auth;
use Validator;

use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller 
{
    
    public $successStatus = 200;

    public $failStatus = 201; 

    public $serverError  = 500;
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function __construct()
    {  
      $this->categoryImagePath      = asset(''); 
    }


    /**
     * getProductList api
     *
     * @return \Illuminate\Http\Response
     */

    public function getProductList(Request $request) 
    {       
        try{ 
  
         $postData = $request->all();  

         $pageCount      = $postData['page'] - 1;
         $per_page_count = $postData['per_page_count'];
         $finalPage      =   $per_page_count * $pageCount; 

         $getList = product::with('productimage');

         if(!empty($request->search) && ($request->search != 'null')){
            $search = $request->search;
            if(!empty($getList)){
                    $getList->where('name', 'like', '%' . $search . '%');
            }
        }

        if(!empty($request->category)&& ($request->category != 'null')){
            $category = $request->category;
            if(!empty($getList)){
                    $getList->where('category_id', $category );
            }
        }

        if(!empty($request->latitude) && !empty($request->longitude)  && ($request->latitude != 'null') && ($request->longitude != 'null')){
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $radius = $request->radius;
            
            $getList->selectRaw("*,
            ( 6371 * acos( cos( radians(" . $latitude . ") ) *
            cos( radians(latitude) ) *
            cos( radians(longitude) - radians(" . $longitude . ") ) + 
            sin( radians(" . $latitude . ") ) *
            sin( radians(latitude) ) ) ) 
            AS distance");
            $getList->having("distance", "<", $radius);           
        }

        $getList = $getList->where("is_active", 1)->whereNull("deleted_at")->skip($finalPage)
        ->take($per_page_count)->get();
      
             if($getList->count() > 0){
                  $listArr = [];
                  $ProductType = config('constants.ProductType');
                  $offerType = getOfferType();
                  $OfferFor = config('constants.OfferFor');

                  foreach($getList as $listVal){
                    $img = [];
                    foreach($listVal->productimage as $key=> $image){
                        $img[$key]['id'] = $image['id'];
                        $img[$key]['image'] = $this->categoryImagePath.$image['image'];
                    }
            
                  $listArr[] = [
                              'id'=>$listVal->id,
                              'category'=>getcategoryName($listVal->category_id),
                              'name'=>$listVal->name,
                              'type'=>$ProductType[$listVal->type],
                              'contact_number'=>$listVal->contact_number,
                              'address'=>$listVal->address,
                              'description'=>$listVal->description,
                              'website_link'=>$listVal->website_link,
                              'offer_type'=>$offerType[$listVal->offer_type],
                              'offer_days'=>getOfferDays($listVal->offer_days),
                              'offer_months'=>getOfferMonths($listVal->offer_months),
                              'call_in_advance'=>$listVal->call_in_advance=='1' ? 'Yes' : 'No',
                              'created_at'=>($listVal->created_at !=null) ? date('Y-m-d',strtotime($listVal->created_at)) : null,
                              'featured_image'=>($listVal['featured_image'] !=null) ? $this->categoryImagePath.$listVal['featured_image'] :null,
                              //'additional_image'=>$listVal->productimage,
                              'additional_images'=>$img,
                  ]; 
              }   
             return response()->json(['success' => true, 'status'=>200,'message'=>'Product list.','data'=>$listArr],$this->successStatus);
  
               }else{
                  return response()->json(['success' => true, 'status'=>201,'message'=>'Product not found.','data'=>[]],$this->successStatus);
               }  
          }catch (Exception $e) {
              $message = 'Caught exception: '.$e->getMessage();
              return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
          }  
      }


      /**
     * getProductList api
     *
     * @return \Illuminate\Http\Response
     */

    public function getProductDetail(Request $request) 
    {       
        try{ 
         $postData = $request->all(); 

         $validator = Validator::make($request->all(), [  
            'product_id' => 'required', 
        ],[ 
            'product_id.required' => 'Product id is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 


         $productDetail = product::with('productimage')->where('id',$postData['product_id'])->where("is_active", 1)->whereNull("deleted_at")->first();

             if($productDetail->count() > 0){
                  $listArr = [];
                  $ProductType = config('constants.ProductType');
                  $offerType = getOfferType();
                  $OfferFor = config('constants.OfferFor');

                  $img = [];
                  foreach($productDetail->productimage as $key=> $image){
                      $img[$key]['id'] = $image['id'];
                      $img[$key]['image'] = $this->categoryImagePath.$image['image'];
                  }
                  $listArr = [
                              'id'=>$productDetail->id,
                              'category'=>getcategoryName($productDetail->id),
                              'name'=>$productDetail->name,
                              'type'=>$ProductType[$productDetail->type],
                              'contact_number'=>$productDetail->contact_number,
                              'address'=>$productDetail->address,
                              'description'=>$productDetail->description,
                              'website_link'=>$productDetail->website_link,
                              'offer_type'=>$offerType[$productDetail->offer_type],
                              'offer_days'=>getOfferDays($productDetail->offer_days),
                              'offer_for'=>$OfferFor[$productDetail->offer_for],
                              'offer_months'=>getOfferMonths($productDetail->offer_months),
                              'call_in_advance'=>$productDetail->call_in_advance=='1' ? 'Yes' : 'No',
                              'created_at'=>($productDetail->created_at !=null) ? date('Y-m-d',strtotime($productDetail->created_at)) : null,
                              'featured_image'=>($productDetail['featured_image'] !=null) ? $this->categoryImagePath.$productDetail['featured_image'] :null,
                              //'additional_images'=>$productDetail->productimage,
                              'additional_images'=> $img,
                  ]; 
             return response()->json(['success' => true, 'status'=>200,'message'=>'Product Detail.','data'=>$listArr],$this->successStatus);
  
               }else{
                  return response()->json(['success' => true, 'status'=>201,'message'=>'Product not found.','data'=>[]],$this->successStatus);
               }
                  
             
  
          }catch (Exception $e) {
              $message = 'Caught exception: '.$e->getMessage();
              return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
          }
      }

    
    
    
  
    /**
     * addInMylistClone api list clone
     *
     * @return \Illuminate\Http\Response
     */

    public function addInMyList(Request $request) { 

      try{ 

       $postData = $request->all(); 

       $validator = Validator::make($request->all(), [  
            'list_id' => 'required', 
        ],[ 
            'list_id.required' => 'List id is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 
 
        $userData         = Auth::user(); 
        $checkListIfExits = Lists::where('is_clone',$postData['list_id'])
                                ->where("created_by", $userData['id'])->first(); 
       if(empty($checkListIfExits)){
            $insertCloneData = Lists::where('id',$postData['list_id'])->first();     
            $addInMyListClone    = New Lists;        
            $addInMyListClone->title                    = $insertCloneData['title'];
            $addInMyListClone->list_text                = $insertCloneData['list_text'];
            $addInMyListClone->post_date                = $insertCloneData['post_date'];        
            $addInMyListClone->category_id              = $insertCloneData['category_id'];
            $addInMyListClone->subcategory_id           = $insertCloneData['subcategory_id'];   
            $addInMyListClone->created_by               = $userData['id'];

            $addInMyListClone->parent_id                 = $insertCloneData['parent_id'];
            //$addInMyListClone->is_clone                  = '';
            
            $addInMyListClone->sponsor_title             = $insertCloneData['sponsor_title'];   
            $addInMyListClone->sponsor_img               = $insertCloneData['sponsor_img']; 

            $addInMyListClone->journey_id               = $insertCloneData['journey_id'];
            $addInMyListClone->is_active                = $insertCloneData['is_active']; 
            if($addInMyListClone->save()){ 

                 $updateIsclone = ['is_clone'=>$addInMyListClone->id];
                 Lists::where('id',$addInMyListClone->id)->update($updateIsclone);

                // insert sub title by parent_id  
                $insertSubTitles = Lists::where('parent_id',$postData['list_id'])->get();
                if(!empty($insertSubTitles)){
                    foreach($insertSubTitles as $insertSubData){   
                        $insertSubTitleClone    = New Lists;        
                        $insertSubTitleClone->title          = $insertSubData['title'];
                        $insertSubTitleClone->list_text      = $insertSubData['list_text'];
                        $insertSubTitleClone->post_date      = $insertSubData['post_date'];        
                        $insertSubTitleClone->category_id    = $insertSubData['category_id'];
                        $insertSubTitleClone->subcategory_id = $insertSubData['subcategory_id'];   
                        $insertSubTitleClone->created_by     = $userData['id'];

                        $insertSubTitleClone->parent_id      = $addInMyListClone->id;
                        $insertSubTitleClone->is_clone       = $insertSubData->id;
                        $insertSubTitleClone->sponsor_title  = $insertSubData['sponsor_title'];   
                        $insertSubTitleClone->sponsor_img    = $insertSubData['sponsor_img']; 

                        $insertSubTitleClone->journey_id     = $insertSubData['journey_id'];
                        $insertSubTitleClone->is_active      = $insertSubData['is_active'];  
                        if($insertSubTitleClone->save()){

                           $updateIsSubTitleclone = ['is_clone'=>$insertSubTitleClone->id];
                           Lists::where('id',$insertSubTitleClone->id)->update($updateIsSubTitleclone);

                            $getListLinkData = ListLink::where('list_id',$insertSubData->id)->get();
                                if(!empty($getListLinkData)){
                                    foreach($getListLinkData as $key=>$itemData){   
                                        $createNewItemList               = New ListLink;  
                                        $createNewItemList->list_id      = $insertSubTitleClone->id;
                                        $createNewItemList->link         = $itemData['link'];
                                        $createNewItemList->save();
                                    }  
                                } 
                         }

                    }  
                } 
             return response()->json(['success' => true, 
                'status'=>200,'message'=>'List added successfully.',  
                'data'=>null,
                ],$this->successStatus); 
            } 
        }else{
            return response()->json(['success' => false, 
                'status'=>201,'message'=>'List already added,Please try another list again.',
                'data'=>null 
                ],$this->failStatus); 
        }

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }

    /**
     * addInMyListSubTitle Cloen api Sub title clone
     *
     * @return \Illuminate\Http\Response
     */

    public function addInMyListSubTitle(Request $request) { 

      try{ 

       $postData = $request->all();  

       $validator = Validator::make($request->all(), [  
            'list_id' => 'required', 
            'list_subtitle_id' => 'required', 
        ],[ 
            'list_id.required' => 'List id is required!', 
            'list_subtitle_id.required' => 'List sub title id is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 
 
        $userData = Auth::user(); 
        $checkListIfExits = Lists::where('id',$postData['list_id'])->where("is_active", 1)->whereNull("deleted_at")->first();  
        if(!empty($checkListIfExits)){
             $checkAlredayAdded = Lists::where('is_clone',$postData['list_id'])->where('created_by',$userData['id'])->where("is_active", 1)->whereNull("deleted_at")->first();  
             if(empty($checkAlredayAdded)){ 
                $addInMyListClone                           = New Lists;        
                $addInMyListClone->title                    = $checkListIfExits['title'];
                $addInMyListClone->list_text                = $checkListIfExits['list_text'];
                $addInMyListClone->post_date                = $checkListIfExits['post_date'];        
                $addInMyListClone->category_id              = $checkListIfExits['category_id'];
                $addInMyListClone->subcategory_id           = $checkListIfExits['subcategory_id'];   
                $addInMyListClone->created_by               = $userData['id'];
                $addInMyListClone->is_clone                 = $postData['list_id'];
                $addInMyListClone->parent_id                = $checkListIfExits['parent_id'];
                $addInMyListClone->sponsor_title            = $checkListIfExits['sponsor_title'];   
                $addInMyListClone->sponsor_img              = $checkListIfExits['sponsor_img']; 

                $addInMyListClone->journey_id               = $checkListIfExits['journey_id'];
                $addInMyListClone->is_active                = $checkListIfExits['is_active'];
                if($addInMyListClone->save()){
                     $insertSubTitles = Lists::where('id',$postData['list_subtitle_id'])->first();
                        if(!empty($insertSubTitles)){ 
                                $insertSubTitleClone    = New Lists;        
                                $insertSubTitleClone->title          = $insertSubTitles['title'];
                                $insertSubTitleClone->list_text      = $insertSubTitles['list_text'];
                                $insertSubTitleClone->post_date      = $insertSubTitles['post_date'];        
                                $insertSubTitleClone->category_id    = $insertSubTitles['category_id'];
                                $insertSubTitleClone->subcategory_id = $insertSubTitles['subcategory_id'];   
                                $insertSubTitleClone->created_by     = $userData['id'];

                                $insertSubTitleClone->parent_id      = $addInMyListClone->id;
                                $insertSubTitleClone->is_clone       = $insertSubTitles->id;
                                $insertSubTitleClone->sponsor_title  = $insertSubTitles['sponsor_title'];   
                                $insertSubTitleClone->sponsor_img    = $insertSubTitles['sponsor_img']; 

                                $insertSubTitleClone->journey_id     = $insertSubTitles['journey_id'];
                                $insertSubTitleClone->is_active      = $insertSubTitles['is_active'];  
                                if($insertSubTitleClone->save()){

                                    $getListLinkData = ListLink::where('list_id',$postData['list_subtitle_id'])->get();
                                        if(!empty($getListLinkData)){
                                            foreach($getListLinkData as $key=>$itemData){   
                                                $createNewItemList               = New ListLink;  
                                                $createNewItemList->list_id      = $insertSubTitleClone->id;
                                                $createNewItemList->link         = $itemData['link'];
                                                $createNewItemList->save();
                                        }
                                    }    
                                } 
                    }

                     return response()->json(['success' => true, 
                        'status'=>200,'message'=>'List sub title added successfully.',  
                        'data'=>null,
                        ],$this->successStatus); 
                     
                 } 

             }else{

                 $checkAlredayAddedSubtitle = Lists::where('is_clone',$postData['list_subtitle_id'])->where('created_by',$userData['id'])->where("is_active", 1)->whereNull("deleted_at")->first();  
                 if(empty($checkAlredayAddedSubtitle)){ 
                      $insertSubTitles = Lists::where('id',$postData['list_subtitle_id'])->first();
                        if(!empty($insertSubTitles)){ 
                                $insertSubTitleClone    = New Lists;        
                                $insertSubTitleClone->title          = $insertSubTitles['title'];
                                $insertSubTitleClone->list_text      = $insertSubTitles['list_text'];
                                $insertSubTitleClone->post_date      = $insertSubTitles['post_date'];        
                                $insertSubTitleClone->category_id    = $insertSubTitles['category_id'];
                                $insertSubTitleClone->subcategory_id = $insertSubTitles['subcategory_id'];   
                                $insertSubTitleClone->created_by     = $userData['id'];

                                $insertSubTitleClone->parent_id      = $checkAlredayAdded->id;
                                $insertSubTitleClone->is_clone       = $insertSubTitles->id;
                                $insertSubTitleClone->sponsor_title  = $insertSubTitles['sponsor_title'];   
                                $insertSubTitleClone->sponsor_img    = $insertSubTitles['sponsor_img']; 

                                $insertSubTitleClone->journey_id     = $insertSubTitles['journey_id'];
                                $insertSubTitleClone->is_active      = $insertSubTitles['is_active'];  
                                if($insertSubTitleClone->save()){

                                    $getListLinkData = ListLink::where('list_id',$insertSubTitles->id)->get();
                                        if(!empty($getListLinkData)){
                                            foreach($getListLinkData as $key=>$itemData){   
                                                $createNewItemList               = New ListLink;  
                                                $createNewItemList->list_id      = $insertSubTitleClone->id;
                                                $createNewItemList->link         = $itemData['link'];
                                                $createNewItemList->save();
                                            }  
                                        }
                                } 
                             }
                      return response()->json(['success' => true, 
                        'status'=>200,'message'=>'List sub title added successfully.',  
                        'data'=>null,
                        ],$this->successStatus); 
                 }else{
                       return response()->json(['success' => false, 
                        'status'=>201,'message'=>'List sub title already added,Please try another title.',  
                        'data'=>[],
                        ],$this->failStatus);                     
                 } 


                 
             }

        }     

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }

    
    /**
     * getMyList api
     *
     * @return \Illuminate\Http\Response
     */

    public function getMyList(Request $request) { 

      try{ 
       $userData = Auth::user(); 
       $getMyList = Lists::where("created_by", $userData['id'])->where("parent_id", null)->where("is_active", 1)->whereNull("deleted_at")->get();  
       if(!empty($getMyList)){
            $listArr = [];
            foreach($getMyList as $listVal){
                    $listArr[] = [
                        'id'=>$listVal->id,
                        'is_checked'=>($listVal->is_checked == 1) ? true : false,
                        'title'=>$listVal->title,
                        'list_text'=>$listVal->list_text,
                        'post_date'=>($listVal->post_date !='') ? date('Y-m-d',strtotime($listVal->post_date)) : null,
                        'parent_id'=>$listVal->parent_id,
                        'category_id'=>$listVal->category_id,
                        'subcategory_id'=>$listVal->subcategory_id,
                        'sponsor_title'=>$listVal->sponsor_title,
                        'sponsor_img'=>($listVal['sponsor_img'] !=null) ? $this->categoryImagePath.$listVal['sponsor_img'] :null,
                        'image'=>($listVal['image '] !=null) ? $this->categoryImagePath.$listVal['image'] :null,
                        'journey_id'=>$listVal->journey_id, 
                    ]; 
                }

        return response()->json(['success' => true, 
                'status'=>200,'message'=>'My list.',  
                'data'=>$listArr
                ],$this->successStatus); 
        }else{
            return response()->json(['success' => false, 
                'status'=>201,'message'=>'Not record found.',
                'data'=>null 
                ],$this->failStatus); 
        }

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }

    /**
     * getMySubTitle api
     *
     * @return \Illuminate\Http\Response
     */

    public function getMySubTitle(Request $request) { 

      try{ 
         $validator = Validator::make($request->all(), [  
            'list_id' => 'required', 
        ],[  
            'list_id.required' => 'List id is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 

       $userData = Auth::user(); 
       $postData = $request->all(); 
       $getMySubTitle = Lists::where("created_by", $userData['id'])->where("parent_id", $postData['list_id'])->where("is_active", 1)->whereNull("deleted_at")->with('listlink')->get();  
       if($getMySubTitle->count() > 0){
         $subListArr = null;
          foreach($getMySubTitle as $listVal){

                $subListArr[] = [
                    'id'=>$listVal->id,
                    'is_checked'=>($listVal->is_checked == 1) ? true : false,
                    'title'=>$listVal->title,
                    'list_text'=>$listVal->list_text,
                    'post_date'=>($listVal->post_date !='') ? date('Y-m-d',strtotime($listVal->post_date)) : null,
                    'parent_id'=>$listVal->parent_id,
                    'category_id'=>$listVal->category_id,
                    'subcategory_id'=>$listVal->subcategory_id,
                    'sponsor_title'=>$listVal->sponsor_title,
                    'sponsor_img'=>($listVal['sponsor_img'] !=null) ? $this->categoryImagePath.$listVal['sponsor_img'] :null,
                    'image'=>($listVal['image'] !=null) ? $this->categoryImagePath.$listVal['image'] :null,
                    'journey_id'=>$listVal->journey_id,
                    'listUrl'=>$listVal->listlink
                ]; 
            } 
        return response()->json(['success' => true, 
                'status'=>200,'message'=>'My sub title.',  
                'data'=>$subListArr
                ],$this->successStatus); 
        }else{
            return response()->json(['success' => false, 
                'status'=>201,'message'=>'Not record found.',
                'data'=>null 
                ],$this->failStatus); 
        }

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }


     /**
     * createUpdateMyList api
     *
     * @return \Illuminate\Http\Response
     */

    public function createUpdateMyList(Request $request) {

        try {

           $postData = $request->all();
           $userData         = Auth::user();  
           if(isset($postData['list_id'])){ 

             $validator = Validator::make($request->all(), [  
                    'title' => 'required',
                    'list_text' => 'required',  
                    
                ],[ 
                    'title.required' => 'title is required!',
                    'list_text.required'   => 'List text is required!', 
                ]); 

                if ($validator->fails()) {
                    return response()->json(['success' => false,'status'=>200,$validator->getMessageBag()->toArray()], $this->failStatus);
                }
               $updateListName = ['title'=>$postData['title'],'list_text'=>$postData['list_text']];
                Lists::where('id',$postData['list_id'])->update($updateListName);
                return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List updated successfully.',  
                    'data'=>null,
                    ],$this->successStatus); 

           }else{
              $validator = Validator::make($request->all(), [  
                    'title' => 'required',
                    'list_text' => 'required', 
                    
                ],[ 
                    'title.required' => 'Title is required!',
                    'list_text.required'   => 'List text is required!',  
                ]); 

                if ($validator->fails()) {
                    return response()->json(['success' => false, 'message'=>$validator->getMessageBag()->toArray(),'data'=>null,], $this->failStatus);
                } 
                $createNewList               = New Lists;        
                $createNewList->title        = $postData['title']; 
                $createNewList->list_text    = $postData['list_text'];  
                $createNewList->created_by   = $userData['id'];  
                if($createNewList->save()){
                    return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List created successfully.',  
                    'data'=>null,
                    ],$this->successStatus); 
                }
           } 

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }

     /**
     * createUpdateMyList api
     *
     * @return \Illuminate\Http\Response
     */

    public function createListSubItem(Request $request) {

        try{
            $postData       = $request->all();  
            $userData       = Auth::user();  
            $validator = Validator::make($request->all(), [  
                    'list_id' => 'required',
                    'title' => 'required',
                    'list_text' => 'required', 
                    
                ],[ 
                    'list_id.required' => 'List id is required!',
                    'title.required' => 'List name is required!',
                    'list_text.required'   => 'List text is required!',  
                ]); 

                if ($validator->fails()) {
                    return response()->json(['success' => false,'status'=>201,'message'=>$validator->getMessageBag()->toArray()], $this->failStatus);
                } 


            if(isset($postData['list_id'])){ 

                 if(!empty($request->file('image'))){    
                    $image = $request->file('image');
                    $fileName = time().'.'.$image->extension(); 
                    $image_name = "uploads/".$fileName; 
                    $img = Image::make($image->getRealPath());
                    $img->resize(440, 296, function ($const) {
                        $const->aspectRatio();
                    })->save($image_name); 
                }else{ 
                    $image_name = '';
                } 

                $createNewItemList               = New Lists;        
                $createNewItemList->title        = $postData['title']; 
                $createNewItemList->list_text    = $postData['list_text'];  
                $createNewItemList->created_by   = $userData['id'];
                $createNewItemList->parent_id    = $postData['list_id'];
                $createNewItemList->image        = $image_name;  
                if($createNewItemList->save()){

                    if(!empty($postData['list_url'])){   
                        foreach($postData['list_url'] as $key=>$itemData){   
                            $createNewItemLink               = New ListLink;  
                            $createNewItemLink->list_id      = $createNewItemList->id;
                            $createNewItemLink->link         = $itemData;
                            $createNewItemLink->save();
                        }
                    } 

                    return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List sub title created successfully.',  
                    'data'=>null,
                    ],$this->successStatus); 
                }
            }else{
                 return response()->json(['success' => false, 
                    'status'=>201,'message'=>'List sub title not created please try again.',  
                    'data'=>null,
                    ],$this->failStatus); 
            } 
        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }


    /**
     * updateSubTitleMyList api
     *
     * @return \Illuminate\Http\Response
     */

    public function updateListSubItem(Request $request) {

        try{
            $postData       = $request->all();    
            $userData       = Auth::user();

            $validator = Validator::make($request->all(), [  
                    'list_subtitle_id' => 'required',
                    'title' => 'required',
                    'list_text' => 'required', 
                    
                ],[ 
                    'list_subtitle_id.required' => 'List sub title id is required!',
                    'title.required' => 'List name is required!',
                    'list_text.required'   => 'List text is required!',  
                ]); 

                if ($validator->fails()) {
                    return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
                } 

            if(isset($postData['list_subtitle_id'])){

                 if(!empty($request->file('image'))){  

                    $listsImage = Lists::where('id',$postData['list_subtitle_id'])->select('image')->first(); 
                    if(!empty($listsImage->image)) {   
                          $image_path = public_path().'/'.$listsImage->image;  
                        if(file_exists($image_path)) { 
                            unlink($image_path);
                        }
                     }  
                    $image = $request->file('image');
                    $fileName = time().'.'.$image->extension(); 
                    $image_name = "uploads/".$fileName; 
                    $img = Image::make($image->getRealPath());
                    $img->resize(440, 296, function ($const) {
                        $const->aspectRatio();
                    })->save($image_name);

                }else{  
                    $returnData = Lists::where('id',$postData['list_subtitle_id'])->select('image')->first();
                    $image_name = $returnData['image'];
                } 

               $updateListDetails = [
                    'title'         =>$postData['title'],
                    'list_text'     =>$postData['list_text'],
                    'post_date'     =>date('Y-m-d'),
                    'image'         =>$image_name,
                    'created_by'    =>$userData['id'], 
                ];  

              Lists::where('id',$postData['list_subtitle_id'])->update($updateListDetails);

              if(!empty($postData['list_url'])){  
                    DB::table('list_links')->where('list_id', $postData['list_subtitle_id'])->delete();
                    foreach($postData['list_url'] as $key=>$itemData){   
                        $createNewItemList               = New ListLink;  
                        $createNewItemList->list_id      = $postData['list_subtitle_id'];
                        $createNewItemList->link         = $itemData;
                        $createNewItemList->save();
                    }
                }
                return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List sub title updated successfully.',  
                    'data'=>[],
                    ],$this->successStatus);  

            }else{
                 return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List sub title not created please try again.',  
                    'data'=>null,
                    ],$this->successStatus); 
            } 
        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }


    /**
     * deleteMyList api
     *
     * @return \Illuminate\Http\Response
     */

    public function deleteMyList(Request $request) {

        try{

            $postData       = $request->all();    
            $userData       = Auth::user();
            $validator = Validator::make($request->all(), [  
                    'list_id' => 'required', 
                ],[ 
                    'list_id.required' => 'List id is required!', 
                ]); 

                if ($validator->fails()) {
                    return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
                }  
                 $deletListPids = Lists::where('id',$postData['list_id'])->get();   
                 if($deletListPids->count() > 0){ 
                    $subTitleIds = [];
                    $getListPids = Lists::where('parent_id',$postData['list_id'])->get();
                    foreach($getListPids as $deleteSubTitle){
                        $subTitleIds[] = $deleteSubTitle->id; 
                        DB::table('list_links')->where('list_id', $deleteSubTitle->id)->delete();
                        DB::table('lists')->where('id', $deleteSubTitle->id)->delete(); 
                    } 
                     DB::table('lists')->where('id', $postData['list_id'])->delete(); 
                     return response()->json(['success' => true, 
                        'status'=>200,'message'=>'List deleted successfully.',  
                        'data'=>null,
                        ],$this->successStatus); 
                 }else{ 
                     return response()->json(['success' => false, 
                        'status'=>201,'message'=>'List not deleted please try again.',  
                        'data'=>null,
                        ],$this->successStatus); 
                 } 

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }

    /**
     * deleteMyListSubItem api
     *
     * @return \Illuminate\Http\Response
     */

    public function deleteMyListSubItem(Request $request) {

        try{
            $postData       = $request->all();    
            $userData       = Auth::user();

            $validator = Validator::make($request->all(), [  
                    'list_subtitle_id' => 'required', 
                ],[ 
                    'list_subtitle_id.required' => 'List sub title id is required!', 
                ]); 

                if ($validator->fails()) {
                    return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
                } 

             DB::table('lists')->where('id', $postData['list_subtitle_id'])->delete();

              return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List sub title deleted successfully.',  
                    'data'=>null,
                    ],$this->successStatus); 


        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }

    }


    /**
     * ParentisChecked status api
     *
     * @return \Illuminate\Http\Response
     */

    public function ListIsChecked(Request $request) {

         try{

            $postData  = $request->all();   
            $validator = Validator::make($request->all(), [  
                 'list_id' => 'required', 
            ],[ 
                'list_id.required' => 'List id is required!', 
            ]); 

            if($validator->fails()) {
                return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
            } 

             $listsIdExist = Lists::where('parent_id',$postData['list_id'])->get(); 
             if(!empty($listsIdExist)){
                $getParentids = [];
                foreach($listsIdExist as $updateIsChecked){  
                     $getParentids[] = $updateIsChecked['id'];
                }  
               Lists::whereIn('id',$getParentids)->update(['is_checked'=>$postData['is_checked']]);
               Lists::where('id',$postData['list_id'])->update(['is_checked'=>$postData['is_checked']]);

               $updatedStatus = [  
                    'is_checked'=>$postData['is_checked']
                ];
                return response()->json(['success' => true, 
                    'status'=>200,'message'=>'List status updated successfully.',  
                    'data'=>$updatedStatus,
                    ],$this->successStatus); 

             }else{
                   return response()->json(['success' => false, 
                    'status'=>201,'message'=>'Record not found.',  
                    'data'=>null,
                    ],$this->failStatus); 
             }

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }
    }


     /**
     * ParentisChecked status api
     *
     * @return \Illuminate\Http\Response
     */

    public function ListItemIsChecked(Request $request) {

         try{
            
            $postData  = $request->all();   
            $validator = Validator::make($request->all(), [  
                 'item_id' => 'required', 
            ],[ 
                'item_id.required' => 'item id is required!', 
            ]); 

            if($validator->fails()) {
                return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
            } 

             $itemIdExist = Lists::where('id',$postData['item_id'])->first(); 
             if(!empty($itemIdExist)){
               $parent_id    = $itemIdExist['parent_id'];
               $totalNonChecked = Lists::where('parent_id',$parent_id)->get(); 
               $totalChecked = Lists::where('parent_id',$parent_id)->where('is_checked',1)->get();
                if($totalNonChecked->count() == $totalChecked->count()){ 
                    Lists::where('id',$parent_id)->update(['is_checked'=>$postData['is_checked']]);
                }else{
 
                    Lists::where('id',$postData['item_id'])->update(['is_checked'=>$postData['is_checked']]);

                     $totalNonChecked = Lists::where('parent_id',$parent_id)->get(); 
                     $totalChecked = Lists::where('parent_id',$parent_id)->where('is_checked',1)->get();

                    if($totalNonChecked->count() == $totalChecked->count()){ 
                        Lists::where('id',$parent_id)->update(['is_checked'=>$postData['is_checked']]);
                     }

                } 
                $updatedStatus = [  
                    'is_checked'=>$postData['is_checked']
                ];
                  return response()->json(['success' => true, 
                    'status'=>200,'message'=>'Status updated successfully.',  
                    'data'=>$updatedStatus,
                    ],$this->successStatus); 
             
             }else{
                   return response()->json(['success' => false, 
                    'status'=>201,'message'=>'Record not found.',  
                    'data'=>null,
                    ],$this->failStatus); 
             } 

        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }
    }


    



    

















}
