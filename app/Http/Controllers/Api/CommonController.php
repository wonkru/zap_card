<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\CmsPages;
use App\Models\Enquirys;
use App\Models\PickList;
use App\Models\OfferType;
use App\Models\MembershipPlan;
use App\Domains\Auth\Models\User;
use App\Models\Cuisine;
use Mail;
use Auth;
use Validator;
use App\Models\Cms\Notifications;

class CommonController extends Controller 
{
    public $successStatus = 200;

    public $failStatus = 201; 

    public $serverError  = 500;

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function __construct()
    {
        #$this->userRepository = $userRepository;
    }

    public function getCuisines(){ 

        $cusinesList = Cuisine::where("is_active", 1)->whereNull("deleted_at")->get()->toArray();
        
        if(!empty($cusinesList) && $cusinesList !=''){ 
            return response()->json(['success' => true, 'status'=>200,'message'=>'Cuisines List','data'=>$cusinesList], 200);
        }else{
            return response()->json(['success' => false, 'status'=>201,'message'=>'No record found','data'=>false ], 201);
        }        
    } 

    public function getOfferType(){ 

        $offerList = OfferType::where("is_active", 1)->whereNull("deleted_at")->get()->toArray();
        
        if(!empty($offerList) && $offerList !=''){ 
            return response()->json(['success' => true, 'status'=>200,'message'=>'Offer Type List','data'=>$offerList], 200);
        }else{
            return response()->json(['success' => false, 'status'=>201,'message'=>'No record found','data'=>false ], 201);
        }        
    } 

    public function getMembershipPlan(){ 

        $membershipPlan = MembershipPlan::where("is_active", 1)->whereNull("deleted_at")->get();
        $planType = config('constants.PlanType');
        $discountType = config('constants.DiscountType');
        foreach($membershipPlan as $listVal){           
            $listArr[] = [
                      'id'=>$listVal->id,
                      'plan_name'=>$listVal->plan_name,
                      'plan_type'=>$planType[$listVal->plan_type],
                      'amount'=>$listVal->amount,
                      'plan_details'=>$listVal->plan_details,
                      'discount_type'=>$discountType[$listVal->discount_type],
                      'discount_amount'=>$listVal->discount_amount,
                      'number_of_day'=>$listVal->number_of_day,
                      'is_active'=>$listVal->is_active,
                      'created_at'=>($listVal->created_at !=null) ? date('Y-m-d',strtotime($listVal->created_at)) : null,
            ]; 
        } 
        
        if(!empty($membershipPlan) && $membershipPlan !=''){ 
            return response()->json(['success' => true, 'status'=>200,'message'=>'Membership Plan List','data'=>$listArr], 200);
        }else{
            return response()->json(['success' => false, 'status'=>201,'message'=>'No record found','data'=>false ], 201);
        }        
    } 
  
}
