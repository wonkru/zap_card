<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
//use Illuminate\Http\Response;

use App\Http\Controllers\Controller;

use App\Models\Cms\CmsPages;
use App\Models\Cms\Enquirys;
use App\Models\Cms\PickList;
use App\Models\Cms\UserRegions;
use App\Models\Cms\UserRegionsGP;
use App\Models\Cms\UserGenralInsurances;
use App\Models\Cms\UserPassports;
use App\Models\Cms\UserDrivingLicense;
use App\Models\Cms\UserRegionsVechiles;
use App\Models\Cms\VehicleFinanceAgreement;
use App\Models\OauthAccessToken;
use App\Models\TitleList;
use App\Models\RelationshipList;
use App\Models\UserChildDob;
use App\Models\UserAddress;
use App\Models\UserMedicalHub;
use App\Models\MedicalHubContact; 
use App\Models\Cms\Lists;
use App\Models\Country;
use App\Models\State;





use Illuminate\Support\Str; 

use App\Domains\Auth\Models\User;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Mail;
use URL;
use DB;
use Auth;
use Validator;
use App\Models\Cms\Notifications;

class UserController extends Controller 
{
    public $successStatus = 200;

    public $failStatus = 201; 

    public $serverError  = 500;
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function __construct()
    {
        #$this->userRepository = $userRepository;
 
          $this->defaultUserImagePath   = asset('img/deafault.png');

    }

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request) { 
    
       $postData = $request->all();
 
        $validator = Validator::make($request->all(), [  
            'email'                 => 'required',
            'password'              => 'required',
            //'device_id'             => 'required', // user login combination 
        ],[
            'email.required' => 'Email is required!',
            'password.required' => 'Password is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], 200);
        }

        if(Auth::attempt(['email' => request('email'),'password' => request('password'),'active'=> 1])){
             $userData = Auth::user();  

             if($userData->active == 0) { 
                $success['token'] =  '';
                return response()->json(['success' => false, 'status'=>200,'message'=>'Your account is not active'],201);
            } /*else if($userData->confirmed == 0) {
                
                $success['token'] =  '';
                return response()->json(['success' => false, 'status'=>200,'message'=>'Your account is not confirmed. Please click the confirmation link in your e-mail.'],200);
         }*/else{

              $userData = Auth::user();            
              $userData['token'] =  $userData->createToken('name')->accessToken;  
              

              if(isset($postData['device_type'])){
               User::where('id',$userData['id'])->update(['device_firebase_token'=> $postData['device_firebase_token'] ]);
              }

              $imagePath =  asset('').$userData['image'];   
              
              $successData =  [
                     "userId"               => $userData['id'], 
                     "firstName"            => $userData['first_name'], 
                     "lastName"             => $userData['last_name'],
                     "email"                => $userData['email'],
                     "mobile_number"        => $userData['mobile_number'],
                     "accessToken"          => $userData['token'],
                     "userImage"            => ($userData['image'] !='') ? $imagePath:$this->defaultUserImagePath, 
                     "is_mobile_verified"   => $userData['is_mobile_verified']=='1'?true:false, 
                   ];
                return response()->json([
                    'success' => true, 
                    'message'=>'User logged in successfuly',
                    'data'=>$successData
                ],$this->successStatus); 
        }
    }else{ 
            if(empty(User::where("email", request('email'))->first())){
                return response()->json(['success' => false,'status'=>201, 'message'=>'Your email is not matching our records please check the spelling, spacing or try another email address.','data'=>null ] ,$this->failStatus);
            }elseif(empty(User::where("password", request('password'))->first())){
                return response()->json(['success' => false,'status'=>201, 'message'=>'Your password does not match your email address, please check the spelling, spacing or use the link below.','data'=>null ] ,$this->failStatus);
            }else{
                 return response()->json(['success' => false,'status'=>201, 'message'=>'Incorrect Username and Password','data'=>null ] ,$this->failStatus);
            }
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */


    public function register(Request $request)
    {
        $postData = $request->all(); 

        $validator = Validator::make($request->all(), [ 
            'first_name'            => 'required', 
            'last_name'             => 'required', 
            'mobile_number'         => 'required', 
            'email'                 => 'required|email',
            'address'               => 'required', 
            'city'                  => 'required', 
            'postcode'              => 'required', 
            'password'              => 'required',
            //'device_id'             => 'required', // user login combination
            //'device_type'           => 'required', // 1 = Android , 2 = IOS
            //'device_firebase_token' => 'required', // for send notification on device 
        ],[
            'email.required' => 'Email is required!',
            'password.required' => 'Password is required!',
            //'device_id.required' => 'Device id is required!',
            //'device_type.required' => 'Device type is required!',
            //'device_firebase_token.required' => 'Firebase token is required!',
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }

      
        $isExits = User::where("email", $postData['email'])->whereNull("deleted_at")->first(); 
        $isExitsMobile = User::where("mobile_number", $postData['mobile_number'])->whereNull("deleted_at")->first(); 

       
        if(!empty($isExits)){
            $errors['email'] = 'Email address is already exits.';            
            return response()->json(['success' => false, 'message' => 'Email address is already exits.'], $this->successStatus);
        } 

        if(!empty($isExitsMobile)){
            $errors['Mobile_number'] = 'Mobile Number is already exits.';            
            return response()->json(['success' => false, 'message' => 'Mobile Number is already exits.'], $this->successStatus);
        }        

        $newUser = New User;        
        $newUser->first_name                = $postData['first_name'];
        $newUser->last_name                 = $postData['last_name'];
        $newUser->email                     = $postData['email'];
        $newUser->mobile_number             = $postData['mobile_number'];
        $newUser->address                   = $postData['address'];
        $newUser->city                      = $postData['city'];
        $newUser->country                   = '235';
        $newUser->postcode                  = $postData['postcode'];
        $newUser->password                  = Hash::make($postData['password']);
        $newUser->device_id             = isset($postData['device_id']) ? $postData['device_id'] : null;
        $newUser->device_type           = isset($postData['device_type']) ? $postData['device_type'] : null;
        $newUser->device_firebase_token = isset($postData['device_firebase_token']) ? $postData['device_firebase_token'] : null;  

        $newUser->active = 1;
        $newUser->confirmed = 0;

        if($newUser->save()){

                $newUser->sendEmailVerificationNotification();
                $successData =[
                        "userId" => $newUser->id,  
                        "firstName" => $newUser->first_name,  
                        "lastName" => $newUser->last_name,  
                        "email" => $newUser->email, 
                        "mobile_number" => $newUser->mobile_number, 
                        "userImage"  => $this->defaultUserImagePath,
                    ]; 
            return response()->json([
                'success' => true, 
                'message'=>'Your account has been created successfully',
                'data'=>$successData
            ],$this->successStatus);   
        } 
    }


   /**
     * Logout api
     *
     * @return \Illuminate\Http\Response
    */

    public function logout(Request $request){ 
        $userData = Auth::user();  

        $carDetail = OauthAccessToken::where('user_id', $userData['id'])->where('revoked', 0)
         ->update(['revoked'=> 1]);  
        $user = User::find($userData['id']);
        $user->device_id = '';
        $user->device_firebase_token = '';
        $user->save(); 
        return response()->json(['success' => true,'status' => 200,'message'=>'Successfully Logout'],$this->successStatus);
    }

    /**
     * sendOtp api
     *
     * @return \Illuminate\Http\Response
    */

    public function sendOtp(Request $request){
        
        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [  
            'mobile_number'                 => 'required',  
            'user_id'                 => 'required',  
        ],[
            'mobile_number.required' => 'Mobile Number is required!',
            'user_id.required' => 'User Id is required!',
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }        
        
        $isExitsMobile = User::where("mobile_number", $postData['mobile_number'])->where('id',$postData['user_id'])->whereNull("deleted_at")->first(); 

        $isAlreadyMobile = User::where("mobile_number", $postData['mobile_number'])->where('id','!=',$postData['user_id'])->whereNull("deleted_at")->first();
        
        $isUserAnotherMobile = User::where('id',$postData['user_id'])->where('mobile_number','!=',$postData['mobile_number'])->whereNull("deleted_at")->first(); 

        if(!empty($isAlreadyMobile)){
           
            $errors['Mobile_number'] = 'This mobile number is registered with another user.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This mobile number is registered with another user.', 'data'=>null],$this->failStatus);
        }

       /* if(empty($isExitsMobile)){
            $errors['Mobile_number'] = 'This mobile number is not registered with us.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This mobile number is not registered with us.', 'data'=>null],$this->failStatus);
        } */

        if($isUserAnotherMobile){
        
            $updateOtp = [
              //'otp'    => rand(1000,9999),
              'otp'    => '1234',
              ];  
             
          $updateOtp = User::where('id',$postData['user_id'])->update($updateOtp);
          return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully','data'=>null],$this->successStatus );
        }

        if($isAlreadyMobile){          
            $updateOtp = [
              //'otp'    => rand(1000,9999),
              'otp'    => '1234',
              ];  
             
          $updateOtp = User::where('id',$postData['user_id'])->update($updateOtp);
          return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully','data'=>null],$this->successStatus );
          } 
            
        if($isExitsMobile){        
          $updateOtp = [
            //'otp'    => rand(1000,9999),
            'otp'    => '1234',
            ];
           
        $updateOtp = User::where('id',$isExitsMobile->id)->update($updateOtp);
        return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully','data'=>null],$this->successStatus );
        } 
    }

    /**
     * verifyOtp api
     *
     * @return \Illuminate\Http\Response
    */

    public function verifyOtp(Request $request){
        
        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [  
            'user_id'       => 'required',  
            'mobile_number'       => 'required',  
            'otp'                 => 'required',  
        ],[
            'user_id.required' => 'User Id is required!',
            'mobile_number.required' => 'Mobile Number is required!',
            'otp.required' => 'Otp is required!',
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }        
        
        $usaeData = User::where("id", $postData['user_id'])->whereNull("deleted_at")->first(); 

        if(empty($usaeData)){
            $errors['Mobile_number'] = 'This user is not registered with us.';            
            return response()->json(['success' => false,  'status'=>201,'message' => 'This user is not registered with us.','data'=>null],$this->failStatus);
        } 

        if($usaeData){
            if($usaeData->otp == $postData['otp']){
                $updateOtp = [
                    //'otp'    => rand(100000,999999),
                    'otp'    => null,
                    'is_mobile_verified'    => '1',
                    'mobile_number'    => $postData['mobile_number'],
                    ]; 

                 
                $updateOtp = User::where('id',$usaeData->id)->update($updateOtp);

                $userData = User::where('id',$usaeData->id)->first();
                $imagePath =  asset('').$userData['image'];   
                $successData =[
                    "userId"               => $userData['id'], 
                    "firstName"            => $userData['first_name'], 
                    "lastName"             => $userData['last_name'],
                    "email"                => $userData['email'],
                    "mobile_number"        => $userData['mobile_number'],
                    "userImage"            => ($userData['image'] !='') ? $imagePath:$this->defaultUserImagePath, 
                    "is_mobile_verified"   => $userData['is_mobile_verified']=='1'?true:false, 
                ];
                return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp verified successfully','data'=>$successData],$this->successStatus );               

            }else{
                return response()->json(['success' => false, 'status'=>201,'message' => 'Invalid Otp','data'=>null],$this->failStatus);
            }
        } 
    }


    /**
     * updateUserMobile api
     *
     * @return \Illuminate\Http\Response
    */

    public function updateUserMobile(Request $request){

      
        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [  
            'mobile_number'           => 'required',   
        ],[
            'mobile_number.required' => 'Mobile Number is required!',
        ]);
        
        $userData = Auth::user(); 

        
        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }        
        
        $isExitsMobile = User::where("mobile_number", $postData['mobile_number'])->where('id',$userData->id)->whereNull("deleted_at")->first(); 

        $isAlreadyMobile = User::where("mobile_number", $postData['mobile_number'])->where('id','!=',$userData->id)->whereNull("deleted_at")->first();
        
        $isUserAnotherMobile = User::where('id',$userData->id)->where('mobile_number','!=',$postData['mobile_number'])->whereNull("deleted_at")->first(); 

        if(!empty($isAlreadyMobile)){
           
            $errors['Mobile_number'] = 'This mobile number is registered with another user.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This mobile number is registered with another user.', 'data'=>null],$this->failStatus);
        }

       /* if(empty($isExitsMobile)){
            $errors['Mobile_number'] = 'This mobile number is not registered with us.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This mobile number is not registered with us.', 'data'=>null],$this->failStatus);
        } */

        if($isUserAnotherMobile){
        
            $updateOtp = [
              //'otp'    => rand(1000,9999),
              'otp'    => '1234',
              'is_mobile_verified'    => '0',
              ];  
             
          $updateOtp = User::where('id',$userData->id)->update($updateOtp);
          return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully','data'=>null],$this->successStatus );
        }

        if($isAlreadyMobile){          
            $updateOtp = [
              //'otp'    => rand(1000,9999),
              'otp'    => '1234',
              'is_mobile_verified'    => '0',
              ];  
             
          $updateOtp = User::where('id',$userData->id)->update($updateOtp);
          return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully','data'=>null],$this->successStatus );
          } 
            
        if($isExitsMobile){        
            $errors['Mobile'] = 'This mobile number is same with previous mobile number.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This mobile number is same with previous mobile number.', 'data'=>null],$this->failStatus);
        } 
    }


     /**
     * verifyMobileOtp api
     *
     * @return \Illuminate\Http\Response
    */

    public function verifyMobileOtp(Request $request){

        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [ 
            'mobile_number'       => 'required',  
            'otp'         => 'required',  
        ],[
            'mobile_number.required' => 'Mobile Number is required!',
            'otp.required' => 'Otp is required!',
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }        
        
        $userData = Auth::user();  

        if(empty($userData)){
            $errors['Mobile'] = 'This user is not registered with us.';            
            return response()->json(['success' => false,  'status'=>201,'message' => 'This user is not registered with us.','data'=>null],$this->failStatus);
        } 

        if($userData){
            if($userData->otp == $postData['otp']){
                $updateOtp = [
                    'otp'    => null,
                    'is_mobile_verified'    => '1',
                    'mobile_number'    => $postData['mobile_number'],
                    ]; 

                 
                $updateOtp = User::where('id',$userData->id)->update($updateOtp);

                $userData = User::where('id',$userData->id)->first();
                $imagePath =  asset('').$userData['image'];   
                $successData =[
                    "userId"               => $userData['id'], 
                    "firstName"            => $userData['first_name'], 
                    "lastName"             => $userData['last_name'],
                    "email"                => $userData['email'],
                    "mobile_number"        => $userData['mobile_number'],
                    "userImage"            => ($userData['image'] !='') ? $imagePath:$this->defaultUserImagePath, 
                    "is_mobile_verified"   => $userData['is_mobile_verified']=='1'?true:false, 
                ];
                return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp verified successfully','data'=>$successData],$this->successStatus );               

            }else{
                return response()->json(['success' => false, 'status'=>201,'message' => 'Invalid Otp','data'=>null],$this->failStatus);
            }
        } 
    }

   /**
     * updateUserEmail api
     *
     * @return \Illuminate\Http\Response
    */

    public function updateUserEmail(Request $request){
        
        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [  
            'email'                 => 'email|required',
        ],[
            'email.required' => 'Email is required!',            
        ]); 

        $userData = Auth::user(); 
      
        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }        
        
        $isExitsEmail = User::where("email", $postData['email'])->where('id',$userData->id)->whereNull("deleted_at")->first(); 

        $isAlreadyEmail = User::where("email", $postData['email'])->where('id','!=',$userData->id)->whereNull("deleted_at")->first();
        
        $isUserAnotherEmail = User::where('id',$userData->id)->where('email','!=',$postData['email'])->whereNull("deleted_at")->first(); 

        if(!empty($isAlreadyEmail)){
           
            $errors['Email'] = 'This email is registered with another user.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This email is registered with another user.', 'data'=>null],$this->failStatus);
        }

        if(!empty($isExitsEmail)){

            $errors['Email'] = 'This email is same with previous email.';            
            return response()->json(['success' => false,  'status'=>201, 'message' => 'This email is same with previous email.', 'data'=>null],$this->failStatus);
        }       

        if($isUserAnotherEmail){           
            $updateOtp = [
              //'otp'    => rand(1000,9999),
              'otp'    => '1234',
              'is_email_verified'    => '0',
              ]; 
              
            
            $dar = array('name'=> $postData['email'],'otp'=>$updateOtp['otp']);

            $str = $postData['email'];
 
            $response = Mail::send('frontend.mail.email-otp', $data = array('data'=>$dar), function($message) use ($str) {
             $message->to($str)
                ->subject('Otp for email verification');
            });

             
          $updateOtp = User::where('id',$userData->id)->update($updateOtp);
          return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully to email','data'=>null],$this->successStatus );
        }

        if($isAlreadyEmail){          
            $updateOtp = [
              //'otp'    => rand(1000,9999),
              'otp'    => '1234',
              'is_email_verified'    => '0',
              ];  

              $dar = array('name'=> $postData['email'],'otp'=>$updateOtp['otp']);

              $str = $postData['email'];
   
              $response = Mail::send('frontend.mail.email-otp', $data = array('data'=>$dar), function($message) use ($str) {
               $message->to($str)
                  ->subject('Otp for email verification');
              });
             
          $updateOtp = User::where('id',$postData['user_id'])->update($updateOtp);
          return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp sent successfully to email','data'=>null],$this->successStatus );
          } 
    }


     /**
     * verifyEmailOtp api
     *
     * @return \Illuminate\Http\Response
    */

    public function verifyEmailOtp(Request $request){

        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [ 
            'email'       => 'required|email',  
            'otp'         => 'required',  
        ],[
            'email.required' => 'Email is required!',
            'otp.required' => 'Otp is required!',
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        }        
        
        $userData = Auth::user();  

        if(empty($userData)){
            $errors['Email'] = 'This user is not registered with us.';            
            return response()->json(['success' => false,  'status'=>201,'message' => 'This user is not registered with us.','data'=>null],$this->failStatus);
        } 

        if($userData){
            if($userData->otp == $postData['otp']){
                $updateOtp = [
                    'otp'    => null,
                    'is_email_verified'    => '1',
                    'email'    => $postData['email'],
                    ]; 

                 
                $updateOtp = User::where('id',$userData->id)->update($updateOtp);

                $userData = User::where('id',$userData->id)->first();
                $imagePath =  asset('').$userData['image'];   
                $successData =[
                    "userId"               => $userData['id'], 
                    "firstName"            => $userData['first_name'], 
                    "lastName"             => $userData['last_name'],
                    "email"                => $userData['email'],
                    "mobile_number"        => $userData['mobile_number'],
                    "userImage"            => ($userData['image'] !='') ? $imagePath:$this->defaultUserImagePath, 
                    "is_email_verified"   => $userData['is_email_verified']=='1'?true:false, 
                ];
                return response()->json(['success' => true, 'status'=>200, 'message'=>'Otp verified successfully','data'=>$successData],$this->successStatus );               

            }else{
                return response()->json(['success' => false, 'status'=>201,'message' => 'Invalid Otp','data'=>null],$this->failStatus);
            }
        } 
    }


     /**
     * getUserProfile api
     *
     * @return \Illuminate\Http\Response
    */

    public function getUserProfile(Request $request){
        
        $userData = Auth::user();   
        $imagePath =  asset('');   
        if(!empty($userData)){             
               $getProfileData = [
                    'user_id'           =>  $userData['id'],
                    'firstName'        =>  $userData['first_name'],
                    'lastName'         =>  $userData['last_name'],
                    'email'             =>  $userData['email'],
                    'mobile_number'     =>  $userData['mobile_number'],
                    'address'           =>  $userData['address'],
                    'country'           =>  getCountryName($userData['country']),
                    'city'              =>  $userData['city'],
                    'postcode'          =>  $userData['postcode'],
                    'userImage'     =>  ($userData['image'] !='') ? $imagePath.'storage/'.$userData['image'] : $this->defaultUserImagePath, 
               ]; 

            return response()->json(['success' => true, 'status'=>200, 'message'=>'User profiles.','data'=>$getProfileData], $this->successStatus);
        }else{
            return response()->json(['success' => false, 'status'=>201, 'message'=>'User profile not found.','data'=>null], $this->failStatus);
        }
    } 


    /**
     * updateProfile api
     *
     * @return \Illuminate\Http\Response
    */

    public function updateProfile(Request $request){
        
        $postData = $request->all(); 
        $validator = Validator::make($request->all(), [  
            'first_name'                    => 'required',
            'last_name'                     => 'required',   
            'address'                       => 'required',   
            'mobile_number'                 => 'required',   
            'city'                          => 'required',   
            //'country'                       => 'required',   
            'postcode'                      => 'required',   
        ],[
            'first_name.required'           => 'First name is required!',
            'last_name.required'            => 'Last name is required!', 
            'mobile_number.required'        => 'Mobile Number is required!', 
            'address.required'              => 'Address is required!', 
            'city.required'                 => 'City is required!', 
            //'country.required'              => 'Country is required!', 
            'postcode.required'             => 'Post code is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 

          $userData = Auth::user();    
          if($userData){
          $updateUserDetails = [                
                'first_name'             => $postData['first_name'],
                'last_name'              => $postData['last_name'], 
                'mobile_number'          => $postData['mobile_number'], 
                'address'                => $postData['address'], 
                'city'                   => $postData['city'], 
                //'country'                => $postData['country'], 
                'postcode'               => $postData['postcode'], 
            ]; 

            
         $updatePerUser = User::where('id',$userData->id)->update($updateUserDetails);

         $getProfileData = [
            'user_id'           =>  $userData['id'],
            'firstName'        =>  $userData['first_name'],
            'lastName'         =>  $userData['last_name'],
            'email'             =>  $userData['email'],
            'mobile_number'     =>  $userData['mobile_number'],
            'address'           =>  $userData['address'],
            'country'           =>  getCountryName($userData['country']),
            'city'              =>  $userData['city'],
            'postcode'          =>  $userData['postcode'],
       ]; 
        return response()->json(['success' => true, 'status'=>200, 'message'=>'User profile successfully updated.','data'=>$getProfileData], $this->successStatus);
        }else{
            return response()->json(['success' => false, 'status'=>201, 'message'=>'User profile not updated,Please try again.','data'=>null], $this->failStatus);
        } 
    }





     /**
     * updateUserImage api
     *
     * @return \Illuminate\Http\Response
    */

    public function editProfileImage(Request $request) {

        try {

            $userData = Auth::user();  
            $postData = $request->all();  
            $rules = [
                'image' => ['required','mimes:jpeg,png,jpg,gif','max:5120'],
            ];
        
            $customMessages = [
                'image.required' => 'The profile image is required.',
                'umage.max' => 'The profile image may not be greater than 5 MB.'
            ];
        
            $validator = Validator::make($request->all(), $rules, $customMessages); 
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);
            }
            if (isset($postData['image'])) {

                $userImage = User::where('id', $userData->id)->select('image')->first();
                if(!empty($userImage->image)) {
                      $image_path = public_path().$userImage->image;  
                    if(file_exists($image_path)) { 
                        unlink($image_path);
                    }
                 } 
                 $avatarPath = $request->file('image');
                 $avatarName = time() . '.' . $avatarPath->getClientOriginalExtension();
                 $path = $request->file('image')
                 ->storeAs('uploads/avatar', $avatarName, 'public'); 
                $updateUserProfile = ['image'=>$path];                
                User::where('id',$userData->id)->update($updateUserProfile);

                $imagePath =  asset('');  
                $returnResponse = ['userImage'=>$imagePath.'storage/'.$path,
                 ]; 
                return response()->json(['success' => true, 'status'=>200,'message'=>'User profile updated successfully','data'=>$returnResponse ], $this->successStatus);//,'data'=>$userData
 
             }

        } catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null  ],$this->serverError);
        }
    }

    /**
     * addUserAddress api
     *
     * @return \Illuminate\Http\Response
    */

    public function addUserAddress(Request $request){
        
        $postData = $request->all();       
        $validator = Validator::make($request->all(), [  
            'address'                       => 'required', 
            'city'                          => 'required', 
            'postcode'                      => 'required',   
        ],[
            'address.required'              => 'Address is required!', 
            'city.required'                 => 'City is required!', 
            'postcode.required'             => 'Post code is required!', 
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 

          $userData = Auth::user();
        
          if($userData){

            if(!empty($postData['address_id'])){
                $address_id  = $postData['address_id'];
                
                $updateAddressDetails = [                
                    'street'             => $postData['street'],
                    'address'            => $postData['address'], 
                    'city'               => $postData['city'], 
                    'county'             => $postData['county'], 
                    'postcode'           => $postData['postcode'], 
                    'address_name'       => $postData['address_name'], 
                    'is_default_address' => $postData['is_default_address'], 
                ]; 
                
             $updateUserAddress = UserAddress::where('id',$address_id)->update($updateAddressDetails);
             $updatedAddress  = UserAddress::where('id',$address_id)->first();

             $updatedAddressData = [
                'user_id'           =>  $userData['id'],
                'street'            =>  $updatedAddress['street'],
                'address'           =>  $updatedAddress['address'],
                'city'              =>  $updatedAddress['city'],
                'county'            =>  $updatedAddress['county'],
                'postcode'          =>  $updatedAddress['postcode'],
                'address_name'      =>  $updatedAddress['address_name'],
                'is_default_address'=>  $updatedAddress['is_default_address'],
           ]; 
            
            if($updateUserAddress==1){

                if($postData['is_default_address']=='1'){
                    $addressList = UserAddress::where('user_id',$userData['id'])->where('id','!=',$postData['address_id'])->get();

                    foreach($addressList as $key=>$address){

                        DB::table('user_addressess')
                        ->where('id', $address->id)
                        ->update(['is_default_address' => '0']);
                    }
                }
                 return response()->json(['success' => true, 'status'=>200, 'message'=>'User address successfully updated.','data'=>$updatedAddressData], $this->successStatus);
            }else{
                return response()->json(['success' => false, 'status'=>201, 'message'=>'User address not updated,Please try again.','data'=>null], $this->failStatus);
            }                 
            }

            $newUserAddress = New UserAddress;        
            $newUserAddress->user_id                = $userData['id'];
            $newUserAddress->street                 = $postData['street'];
            $newUserAddress->address                = $postData['address'];
            $newUserAddress->city                   = $postData['city'];
            $newUserAddress->county                 = $postData['county'];
            $newUserAddress->postcode               = $postData['postcode'];
            $newUserAddress->address_name           = $postData['address_name'];
            $newUserAddress->is_default_address     = $postData['is_default_address'];    
            
            if($newUserAddress->save()){

                if($postData['is_default_address']=='1'){
                    $addressList = UserAddress::where('user_id',$userData['id'])->where('id','!=',$newUserAddress->id)->get();

                    foreach($addressList as $key=>$address){

                        DB::table('user_addressess')
                        ->where('id', $address->id)
                        ->update(['is_default_address' => '0']);
                    }
                }

                return response()->json([
                    'success' => true, 
                    'status'=>200,
                    'message'=>'Address saved successfully.',
                    'data'=>null
                ],$this->successStatus);   
    
            }else{
                return response()->json(['success' => false,'status'=>201, 'message'=>'Address not saved.','data'=>null ] ,$this->failStatus);
    
            }
        }
    }

      /**
     * userDeleteAddress api
     *
     * @return \Illuminate\Http\Response
     */


    public function deleteUserAddress(Request $request){
        
        $postData = $request->all(); 
      
        $validator = Validator::make($request->all(), [ 
            'address_id'                 => 'required',   
        ],[
            'address_id.required'        => 'Address Id is required!',
        ]); 

        if ($validator->fails()) {
            return response()->json(['success' => false, $validator->getMessageBag()->toArray()], $this->successStatus);
        } 

          $userData = Auth::user();         
        
          if($userData){

            if(!empty($postData['address_id'])){
                $userAddress  = UserAddress::where('id',$postData['address_id'])->first();
               if(!empty($userAddress)){
                DB::table('user_addressess')->where('id', $postData['address_id'])->update(['deleted_at' => Carbon::now()]);
                return response()->json(['success' => true, 'status'=>200, 'message'=>'User address successfully deleted.','data'=>null], $this->successStatus);

               }else{
                return response()->json(['success' => false, 'status'=>201, 'message'=>'User Address not found.','data'=>null], $this->failStatus);
               }                
            }
        }
    }

    /**
     * getAddressList api
     *
     * @return \Illuminate\Http\Response
     */

    public function getAddressList(Request $request) 
    { 
        try{ 
  
         $postData = $request->all();  
         $userData = Auth::user();

         $addressList = UserAddress::where('user_id',$userData['id'])->whereNull("deleted_at")->get();
      
             if($addressList->count() > 0){
                  $listArr = [];
                 
                  foreach($addressList as $listVal){
                   
                  $listArr[] = [
                              'id'=>$listVal->id,
                              'street'=>$listVal->street,
                              'address'=>$listVal->address,
                              'city'=>$listVal->city,
                              'county'=>$listVal->county,
                              'postcode'=>$listVal->postcode,                              
                              'address_name'=>$listVal->address_name,                              
                              'is_default_address'=>$listVal->is_default_address,
                             'created_at'=>($listVal->created_at !=null) ? date('Y-m-d',strtotime($listVal->created_at)) : null,
                  ]; 
              }   
             return response()->json(['success' => true, 'status'=>200,'message'=>'Address list.','data'=>$listArr],$this->successStatus);
  
               }else{
                  return response()->json(['success' => true, 'status'=>201,'message'=>'Address not found.','data'=>[]],$this->successStatus);
               }  
          }catch (Exception $e) {
              $message = 'Caught exception: '.$e->getMessage();
              return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
          }  
      }
    }


    /**
     * getUserMembership api
     *
     * @return \Illuminate\Http\Response
     */

    public function getUserMembership(Request $request) 
    { 
        try{ 
  
         $postData = $request->all();  
         $userData = Auth::user();

         $userMembership = UserMembership::where('user_id',$userData->id)->with('user','membershipplan')->get(); 
        
             if($userMembership->count() > 0){
                  $listArr = [];
                 
                  foreach($userMembership as $listVal){
                   
                  $listArr[] = [
                              'id'=>$listVal->id,
                              'plan_name'=>$listVal->membershipplan->plan_name,
                              'start_date'=>$listVal->start_date,
                              'end_date'=>$listVal->end_date,
                              'next_due_date'=>$listVal->next_due_date,
                              'created_at'=>($listVal->created_at !=null) ? date('Y-m-d',strtotime($listVal->created_at)) : null,
                  ]; 
              }   
             return response()->json(['success' => true, 'status'=>200,'message'=>'User Membership.','data'=>$listArr],$this->successStatus);
  
               }else{
                  return response()->json(['success' => true, 'status'=>201,'message'=>'Membership not found.','data'=>[]],$this->successStatus);
               }  
          }catch (Exception $e) {
              $message = 'Caught exception: '.$e->getMessage();
              return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
          }  
    }

     /**
     * changePassword api
     *
     * @return \Illuminate\Http\Response
    */

    public function changePassword(Request $request){

       $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:8',
        ]);

       if($validator->fails()){
        return response()->json(['error'=>$validator->errors(),'status'=>401]);
       }else{
         $user = User::find(Auth::user()->id);
         
            if (Hash::check($request->old_password, $user->password)) {

                $user->password_changed_at = now()->toDateTimeString();
                $user->update(['password' => bcrypt($request->password)]);
                //$success['token'] =  $user->createToken('name')->accessToken;
                return response()->json(['success' => true, 'status'=>200, 'message'=>'Password changed successfully','data'=>null], $this->successStatus);
        }else{
            return response()->json(['success' => false, 'status'=>201,'message'=>'Incorrect existing password','data'=>null ],$this->failStatus);
        }
       }
    }
 
     /**
     * getTitleName api
     *
     * @return \Illuminate\Http\Response
    */

    public function getTitleName($titleId){

        $getTitleName = TitleList::where("status",1)->where('id',$titleId)->whereNull("deleted_at")->first(); 

        if(!empty($getTitleName['title'])){
            return $getTitleName['title'];
        }else{
            return $getTitleName['title'];
        }
     }

    public function getDashboardData(Request $request){

        $getIndexCategorys = PickList::where("picklist_id", "category")->where("is_active", 1)->whereNull("deleted_at")->get()->all();
        
        $regionList = UserRegions::with('getRegionName')->where("user_id", Auth::user()->id)->whereNotNull('region_id')->get()->toArray(); /*PickList::where("picklist_id", "region")->where("is_active", 1)->whereNull("deleted_at")->get()->toArray();*/

        $data['category_list'] = $getIndexCategorys;        
        $data['user_region_list'] = $regionList;

        return response()->json(['success' => true, 'message'=>'User databoard data.','data'=>$data], 200);
    } 

    public function resetPassword(Request $request){
        
        $token = DB::table('password_resets')
        ->where('token','=',$request->token)
        //->where('created_at','>',Carbon::now()->subHours(24))
        ->get();       
        
        if(isset($token) && !empty($token) && count($token)>0){

            $existinguser = User::where('email',$token[0]->email)->first();
            
            $existinguser->password_changed_at = now()->toDateTimeString();
            $existinguser->update(['password' => bcrypt($request->password)]);
            $success['token'] =  $existinguser->createToken('name')->accessToken; 

            DB::table('password_resets')->where("token",$request->token)->delete();
            
            return response()->json(['success' => true, 'message'=>'Password Reset successfully','data'=>$success], 200);
        }else{

            return response()->json(['success' => false, 'message'=>'Reset Token has been expired'], 200);
        }
    } 
    

    public function forgot(Request $request)
    {

        $existinguser = User::where('email',$request->email)->first(); 
        if(isset($existinguser->id) && !empty($existinguser->id)){

            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => Str::random(60),
                'created_at' => Carbon::now()
            ]);

            $tokenData = DB::table('password_resets')->where('email', $request->email)->first();

            $dar = array('name'=> $request->email,'link'=>$tokenData->token, 'url'=>URL::to('/') );

            $str = $existinguser->email;
 
            $response = Mail::send('frontend.mail.resetuser', $data = array('data'=>$dar), function($message) use ($str) {
             $message->to($str)
                ->subject('Reset Password Request from Zap Discount');
            });

            if(Mail::failures()){
                return response()->json(['success' => false, 'status'=>201,'message'=>'Email are not sent, contact with support team'], 200);
            }
            return response()->json(['success' => true, 'status'=>200,'message'=>'A reset link has been sent to your email address','data'=>null],200);
        }else{
            //return response()->json(['success' => false,'status'=>201, 'message'=>'You have not registered with us.'], 200);

            return response()->json(['success' => false,'status'=>201, 'message'=>'We do not have this email registered with us. Do you have another email address you might have used when setting up your account?'], 200);
        }                   
    }  

    /**
     * user reset all data api
     *
     * @return \Illuminate\Http\Response
     */


    public function resetData() {

        try {
             $userData = Auth::user();
              $isDeleted = date('Y-m-d H:i:s');
              DB::table('booth_images')->where('user_id', $userData['id'])->delete();  
              DB::table('diary_prompts')->where('created_by', $userData['id'])->delete();

               $getListPids = Lists::where('created_by',$userData['id'])->get();
                if(!empty($getListPids)){
                    foreach($getListPids as $deleteSubTitle){ 
                        DB::table('list_links')->where('list_id', $deleteSubTitle->id)->delete();
                         
                    } 
                 DB::table('lists')->where('created_by', $userData['id'])->delete();
                } 

                DB::table('my_journeys')->where('created_by', $userData['id'])->delete();
                DB::table('my_stuffs')->where('user_id', $userData['id'])->delete();
                DB::table('reminders')->where('user_id', $userData['id'])->delete();
                DB::table('user_child_dobs')->where('user_id', $userData['id'])->delete();
               

                $getmedical_hubsPids = UserMedicalHub::where('user_id',$userData['id'])->get();
                if(!empty($getmedical_hubsPids)){
                    foreach($getmedical_hubsPids as $deleteHubId){ 
                        DB::table('medical_hub_contacts')->where('user_medical_hub_id', $deleteHubId->id)->delete();
                         
                    } 
                  DB::table('user_medical_hubs')->where('user_id', $userData['id'])->delete();
                } 

             return response()->json(['success' => true,'status' => 200,'message'=>'Data resets successfully'],$this->successStatus);
         }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }            
    } 

    /**
     * Country api
     *
     * @return \Illuminate\Http\Response
    */

   public function country(Request $request){
        try {
                $countryList = Country::orderBy('name','ASC')->select('id','name','sortname','phonecode')->get();
                if(!empty($countryList)){
                    return response()->json(['success' => true,'status'=>200, 'message'=>'Country list','data'=>$countryList], $this->successStatus);
                }else{
                    return response()->json(['success' => false,'status'=>201,'message'=>'Country list not found','data'=>null  ],$this->failStatus);
                }
        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        } 
    }

    /**
     * State api
     *
     * @return \Illuminate\Http\Response
    */

   public function state(Request $request){
        try {
            $validator = Validator::make($request->all(), [  
            'country_id' => 'required'
        ],[ 
            'country_id.required' => 'Country id is required!', 
        ]); 
                $postData = $request->all(); 
                $stateList = State::where('country_id',$postData['country_id'])->orderBy('name','ASC')->get();
                if(!empty($stateList)){
                    return response()->json(['success' => true,'status'=>200, 'message'=>'State list','data'=>$stateList], $this->successStatus);
                }else{
                    return response()->json(['success' => false,'status'=>201,'message'=>'State list not found','data'=>null  ],$this->failStatus);
                }
        }catch (Exception $e) {
            $message = 'Caught exception: '.$e->getMessage();
            return response()->json(['success' => false,'status'=>201,'message'=>$message,'data'=>null ] ,$this->failStatus);
        }
    }
}
