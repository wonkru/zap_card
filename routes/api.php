<?php

use Illuminate\Http\Request;
use App\Models\Auth\User;
use App\Http\Controllers\Api\CmsController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CronJobController;
use App\Http\Controllers\Api\NotificationController;

 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get-cmspage', [CmsController::class, 'getCmsPage']); 
Route::post('get-cmspage-byslug', [CmsController::class, 'getPageBySlug']);
Route::get('get-faq', [CmsController::class, 'getFaq']);

//user access section 
Route::post('user/register', [UserController::class, 'register']);
//Route::post('user/sent-verification-code', [UserController::class, 'sentOtp']);
Route::post('user/verify-account', [UserController::class, 'verifyOtp']);
Route::post('user/accept-tandc', [UserController::class, 'accpectTAndC']);
Route::post('user/login', [UserController::class, 'login']);
Route::post('user/forgetpassword', [UserController::class, 'forgot']);
Route::post('user/resetPassword', [UserController::class, 'resetPassword']);

Route::post('user/send-otp', [UserController::class, 'sendOtp']);
Route::post('user/verify-otp', [UserController::class, 'verifyOtp']);
Route::get('user/title-list', [UserController::class, 'titleList']);
Route::get('user/relationship-list', [UserController::class, 'relationshipList']);
Route::get('user/country',  [UserController::class, 'country']);
Route::get('user/state',  [UserController::class, 'state']);
/*Product  Api's*/
Route::post('product/get-product-list', [ProductController::class, 'getProductList']);
Route::post('product/get-product-detail', [ProductController::class, 'getProductDetail']);

// route with user access 


Route::group(['middleware' => 'auth:api'], function() {
	Route::post('user/logout', [UserController::class, 'logout']);
	Route::get('user/get-profile', [UserController::class, 'getUserProfile']); 

	Route::post('user/update-profile', [UserController::class, 'updateProfile']);
	Route::post('user/update-personal-address', [UserController::class, 'updatePersonalAddress']);

	Route::post('user/update-user-email', [UserController::class, 'updateUserEmail']);	
	Route::post('user/verify-email-otp', [UserController::class, 'verifyEmailOtp']);

	Route::post('user/update-profile-image', [UserController::class, 'editProfileImage']);

	Route::post('user/update-user-mobile', [UserController::class, 'updateUserMobile']);	
	Route::post('user/verify-mobile-otp', [UserController::class, 'verifyMobileOtp']);

	Route::post('user/update-profile-image', [UserController::class, 'editProfileImage']);
	Route::post('user/changepassword', [UserController::class, 'changePassword'] );

	Route::post('/user/dashboard', [UserController::class, 'getDashboardData']);	
	Route::post('/user/get-subcategory', [UserController::class, 'getSubcategoryData']);
		
	Route::post('store-enquiries', [CmsController::class, 'storeEnquiries']);  

	Route::post('user/user-onboard', [UserController::class, 'userOnboard']);

	/* User Address Routes */
	Route::post('/user/add-address', [UserController::class, 'addUserAddress']);
	Route::post('/user/delete-address', [UserController::class, 'deleteUserAddress']);
	Route::get('/user/address-list', [UserController::class, 'getAddressList']);

	/* delete account */
	Route::get('user/delete-account', [UserController::class, 'deleteAccount']);
	Route::get('user/reset-data', [UserController::class, 'resetData']);
	Route::post('user/update-notification-status', [UserController::class, 'updateNotificationStatus']);
	Route::get('user/notification-status', [UserController::class, 'notificationStatus']);
	Route::post('notification/get-notification', [NotificationController::class, 'getNotification']);
	Route::post('notification/update-isread', [NotificationController::class, 'updateIsRead']);
	Route::post('notification/notification-status', [NotificationController::class, 'notificationStatus']);

});


/* Cronjob Notification */  
	
	Route::get('cronjob/send-reminder-notification', [CronJobController::class, 'sendReminderNotification']);
	/* send data week wise route*/
	Route::get('cronjob/weekwise-notification', [CronJobController::class, 'weekWiseNotification']);

	Route::post('cronjob/callback-send-notification', [CronJobController::class, 'callbackSendNotification']);
	
